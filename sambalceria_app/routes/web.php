<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\KatalogController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\PemesananController;
use App\Http\Controllers\PembayaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', [
        "title" => "Home"
    ]);
});


Route::get('/login/', [LoginController::class, 'login']);

Route::get('/katalog/', [KatalogController::class, 'katalog']);

Route::get('/register/', [RegisterController::class, 'register']);

Route::get('/pemesanan/', [PemesananController::class, 'pemesanan']);

Route::get('/pembayaran/', [PembayaranController::class, 'pembayaran']);

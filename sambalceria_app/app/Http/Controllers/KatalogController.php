<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Katalog;

class KatalogController extends Controller
{
    public function katalog()
    {
        return view('katalog', [
            "title" => "Katalog",
        ]);
    }
}

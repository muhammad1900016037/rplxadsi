<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemesanan;

class PemesananController extends Controller
{
    public function pemesanan()
    {
        return view('pemesanan', [
            "title" => "Pemesanan",
        ]);
    }
}

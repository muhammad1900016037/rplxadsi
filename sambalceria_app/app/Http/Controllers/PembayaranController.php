<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pembayaran;

class PembayaranController extends Controller
{
    public function pembayaran()
    {
        return view('pembayaran', [
            "title" => "Pembayaran",
        ]);
    }
}

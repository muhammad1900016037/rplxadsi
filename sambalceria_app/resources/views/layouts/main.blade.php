<head>
    <title>Sambal Ceria App | {{ $title }}</title>
</head>
@include('partials.navbar')

        <div class="container mt-4">
            @yield('container')
        </div>
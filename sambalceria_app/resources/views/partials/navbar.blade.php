<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="Dashboard/css/style.css">

	</head>
	<body>
	
			<nav class="navbar navbar-expand-lg ftco_navbar ftco-navbar-light" id="ftco-navbar">
		    <div class="container">
		    	<a class="navbar-brand" href="/">SAMBAL CERIA APP</a>
		    	<div class="social-media order-lg-last">
		    		<p class="mb-0 d-flex">
		    			<a href="https://www.instagram.com/dapur_sambal_/" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
		    		</p>
	        </div>
		      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
		        <span class="fa fa-bars"></span> Menu
		      </button>
		      <div class="collapse navbar-collapse" id="ftco-nav">
		        <ul class="navbar-nav ml-auto mr-md-3">

		        	<li class="nav-item {{ ($title === "Home") ? 'active' : ' ' }}">
						<a href="/" class="nav-link">Home</a></li>
					</li>

		        	<li class="nav-item  {{ ($title === "Katalog") ? 'active' : ' ' }}">
						<a href="/katalog" class="nav-link">Katalog</a></li>
					</li>

		        	<li class="nav-item {{ ($title === "Pemesanan") ? 'active' : ' ' }}">
						<a href="/pemesanan" class="nav-link">Pemesanan</a></li>
					</li>

		        	<li class="nav-item {{ ($title === "Pembayaran") ? 'active' : ' ' }}">
						<a href="/pembayaran" class="nav-link">Pembayaran</a></li>
					</li>

		            <li class="nav-item"><a href="https://goo.gl/maps/rMQ9d77o2maoCwfw7" class="nav-link">Map</a></li>
		        </ul>
		      </div>
		    </div>
		  </nav>
	</body>
</html>


<!doctype html>
<html lang="en">
  <head>
  	<title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="Register/css/style.css">

	</head>
	<body>
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Register</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-7 col-lg-6 col-xl-5">
					<div class="login-wrap p-4 p-md-5">
		      	<div class="icon d-flex align-items-center justify-content-center">
		      		<span class="fa fa-edit"></span>
		      	</div>
		      	<h3 class="text-center mb-4">Buat akunmu di sini</h3>
						<form action="#" class="signup-form">
		      		<div class="form-group mb-3">
		      			<label class="label" for="name">Nama Lengkap</label>
		      			<input type="text" class="form-control" placeholder="John Doe">
		      		</div>
		      		<div class="form-group mb-3">
		      			<label class="label" for="email">Alamat Email</label>
		      			<input type="text" class="form-control" placeholder="johndoe@gmail.com">
		      		</div>
	            <div class="form-group mb-3">
	            	<label class="label" for="password">Password</label>
	              <input id="password-field" type="password" class="form-control" placeholder="Password">
	              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
	            </div>
	            <div class="form-group d-md-flex">
	            	<div class="w-100 text-left">
		            	<label class="checkbox-wrap checkbox-primary">Saya setuju semua pernyataan dalam hal layanan
									  <input type="checkbox" checked>
									  <span class="checkmark"></span>
									</label>
								</div>
	            </div>
	            <div class="form-group">
	            	<button type="submit" class="form-control btn btn-primary rounded submit px-3">Register</button>
	            </div>
	          </form>
	          <p>Sudah mempunyai akun? <a href="/login">Login</a></p>
	        </div>
				</div>
			</div>
		</div>
	</section>

	<script src="Register/js/jquery.min.js"></script>
  <script src="Register/js/popper.js"></script>
  <script src="Register/js/bootstrap.min.js"></script>
  <script src="Register/js/main.js"></script>

	</body>
</html>


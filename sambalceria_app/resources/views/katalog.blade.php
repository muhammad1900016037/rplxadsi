@extends('layouts.main')

@section('container')
<h1>Katalog Produk</h1>

<div class="container">
    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalterasi.jpg" alt="Sambal Terasi">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Terasi</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.45000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalbawang.jpg" alt="Sambal Bawang">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Bawang</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.35000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa grey fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalijo.jpg" alt="Sambal Ijo">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Ijo</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.20000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa grey fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalteri.jpg" alt="Sambal Teri">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Teri</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.50000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa grey fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalcumi.jpg" alt="Sambal Cumi">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Cumi</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.25000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambaldabudabu.jpg" alt="Sambal Dabu Dabu">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Dabu Dabu</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.35000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa grey fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambalmatah.jpg" alt="Sambal Matah">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Matah</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.30000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="imgBx">
            <img src="/images/sambaltempoyak.jpg" alt="Sambal Tempoyak">
        </div>
        <div class="content">
            <div class="productName">
                <h3>Sambal Tempoyak</h3>
            </div>
            <div class="price_rating">
                <h2>Rp.10000</h2>
                <div class="rating">
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i style="color: orange" class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa grey fa-star" aria-hidden="true"></i>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection